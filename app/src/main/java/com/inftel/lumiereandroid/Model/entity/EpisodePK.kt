package com.inftel.lumiereandroid.Model.entity

import java.io.Serializable
import java.math.BigDecimal

data class EpisodePK( var idEpisode: BigDecimal,
         var idSeason: BigDecimal,
         var idSeries: BigDecimal)
