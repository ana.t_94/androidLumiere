package com.inftel.lumiereandroid.Model.entity

import java.math.BigDecimal

data class Userfilmview(
        var userfilmviewPK: UserfilmviewPK,
        var time: BigDecimal,
        var film: Film,
        var users: Users
)