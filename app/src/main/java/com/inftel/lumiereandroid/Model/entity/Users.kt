package com.inftel.lumiereandroid.Model.entity

import java.math.BigDecimal

data class Users(var idUser: BigDecimal?,
                 var name: String?,
                 var pass: String?,
                 var answer: String?,
                 var surname: String?,
                 var email: String?
               )
