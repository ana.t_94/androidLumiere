package com.inftel.lumiereandroid.Model.entity

import java.math.BigDecimal

data class Episode(var episodePK: EpisodePK,
                   var title: String,
                   var duration: BigDecimal,
                   var lifeTime: BigDecimal,
                   var userepisodenofinishCollection: Collection<Userepisodenofinish>,
                   var userepisodeviewCollection: Collection<Userepisodeview>,
                   var season: Season)
