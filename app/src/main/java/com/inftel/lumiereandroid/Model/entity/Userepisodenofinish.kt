package com.inftel.lumiereandroid.Model.entity

import java.math.BigDecimal

data class Userepisodenofinish(
        var userepisodenofinishPK: UserepisodenofinishPK,
        var time: BigDecimal,
        var timestamp: BigDecimal,
        var episode: Episode,
        var users: Users
)
