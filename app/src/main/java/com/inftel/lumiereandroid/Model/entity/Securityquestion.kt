package com.inftel.lumiereandroid.Model.entity

import java.math.BigDecimal

data class Securityquestion( var idQuestion: BigDecimal,
         var question: String,
         var usersCollection: Collection<Users>)
