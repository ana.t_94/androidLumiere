package com.inftel.lumiereandroid.Model.entity

import java.io.Serializable
import java.math.BigDecimal

data class CommentsPK (
                 var idComment: BigDecimal,
                 var idFilm: BigDecimal,
                 var idUser: BigDecimal

)

