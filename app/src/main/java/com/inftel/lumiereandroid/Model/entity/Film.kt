package com.inftel.lumiereandroid.Model.entity

import java.math.BigDecimal

data class Film(var idFilm: BigDecimal,
                var title: String,
                var year: BigDecimal,
                var genres: String,
                var origen: String,
                var rating: Double,
                var ourRating: Double,
                var duration: Double,
                var director: String,
                var imagePoster: String,
                var imageNetflix: String,
                var quote: String,
                var quoteAuthor: String,
                var lifeTime: BigDecimal,
                var sinopsis: String,
                var ytLink: String,
                var usersCollection: Collection<Users>,
                var userfilmnofinishCollection: Collection<Userfilmnofinish>,
                var commentsCollection: Collection<Comments>
)
