package com.inftel.lumiereandroid.Model.entity

import java.math.BigDecimal


data class Userfilmnofinish(var userfilmnofinishPK: UserfilmnofinishPK,
                            var time: BigDecimal,
                            var timestamp: BigDecimal,
                            var film: Film,
                            var users: Users)

