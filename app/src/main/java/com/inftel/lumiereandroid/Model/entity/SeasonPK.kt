package com.inftel.lumiereandroid.Model.entity

import java.io.Serializable
import java.math.BigDecimal

data class SeasonPK(private var idSeason: BigDecimal,
        private var idSeries: BigDecimal)

