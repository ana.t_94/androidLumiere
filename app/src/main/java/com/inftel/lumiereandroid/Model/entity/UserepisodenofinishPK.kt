package com.inftel.lumiereandroid.Model.entity

import java.io.Serializable
import java.math.BigDecimal

data class UserepisodenofinishPK(  var idUser: BigDecimal,
         var idEpisode: BigDecimal,
         var idSeason: BigDecimal,
         var idSeries: BigDecimal
)
