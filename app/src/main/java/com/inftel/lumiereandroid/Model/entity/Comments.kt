package com.inftel.lumiereandroid.Model.entity

data class Comments(var commentsPK: CommentsPK,
                    var content: String,
                    var datePub: String,
                    var film: Film,
                    var users: Users)
