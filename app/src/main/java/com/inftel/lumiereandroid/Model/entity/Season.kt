package com.inftel.lumiereandroid.Model.entity

import java.math.BigDecimal

data class Season(
        var seasonPK: SeasonPK,
        var numberCap: BigDecimal,
        var numberSeason: BigDecimal,
        var episodeCollection: Collection<Episode>,
        var series: Series)
