package com.inftel.lumiereandroid.Model.entity

data class Userepisodeview(
        var userepisodeviewPK: UserepisodeviewPK,
        var timestamp: String,
        var episode: Episode,
        var users: Users)

