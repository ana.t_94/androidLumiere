package com.inftel.lumiereandroid.Model.entity

import java.math.BigDecimal

data class Series( var idSeries: BigDecimal,
         var title: String,
         var sinopsis: String,
         var rating: BigDecimal,
         var origen: String,
         var genders: String,
         var year: BigDecimal,
         var ourRating: BigDecimal,
         var imageNetflix: String,
         var quote: String,
         var quoteAuthor: String,
         var imagePoster: String,
         var lifeTime: BigDecimal,
         var ytLink: String,
         var seasonCollection: Collection<Season>)
