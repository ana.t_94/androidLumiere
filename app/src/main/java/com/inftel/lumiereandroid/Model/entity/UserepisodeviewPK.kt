package com.inftel.lumiereandroid.Model.entity

import java.io.Serializable
import java.math.BigDecimal

data class UserepisodeviewPK( var idUser: BigDecimal,
         var idSeason: BigDecimal,
         var idEpisode: BigDecimal,
         var idSeries: BigDecimal)
