package com.inftel.lumiereandroid.View

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_index.*
import kotlinx.android.synthetic.main.app_bar_index.*
import com.inftel.lumiereandroid.R
import com.inftel.lumiereandroid.Utils.Utility
import com.inftel.lumiereandroid.Adapter.Adapter
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import android.view.animation.AnimationUtils
import android.widget.*
import com.inftel.lumiereandroid.Model.entity.Film
import com.inftel.lumiereandroid.Model.entity.Series
import com.inftel.lumiereandroid.Model.entity.Userepisodenofinish
import com.inftel.lumiereandroid.Model.entity.Userfilmnofinish
import com.inftel.lumiereandroid.Presenter.facade.UserepisodefinishFacade
import com.inftel.lumiereandroid.Presenter.facade.UserfilmnofinishFacade
import com.inftel.lumiereandroid.Presenter.presenter.facade.FilmFacade
import com.inftel.lumiereandroid.Presenter.presenter.facade.SeriesFacade
import com.inftel.lumiereandroid.Utils.HeaderPictures
import kotlinx.android.synthetic.main.content_index.*
import java.util.*


class IndexActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var listSeriesNotFinished: List<Userepisodenofinish>? = null
    var listSeries: List<Series>? = null
    var listFilmsNotFinished: List<Userfilmnofinish>? = null
    var listFilms: List<Film>? = null


    private var imageSwitcher: ImageSwitcher? = null
    private val galeria = intArrayOf(R.drawable.gotham_banner, R.drawable.got, R.drawable.papel)
    private var posicion: Int = 0
    private var timer: Timer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_index)

        toolbar.setBackgroundColor(Color.parseColor("#3b4451"))
        window.statusBarColor = Color.parseColor("#3b4451")

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout_index, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout_index.addDrawerListener(toggle)
        toggle.syncState()

        imageSwitcher = findViewById<View>(R.id.imageSwitcher) as ImageSwitcher
        imageSwitcher!!.setFactory {
            val imageView = ImageView(this)
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP

            imageView
        }
        val fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        val fadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out)
        imageSwitcher!!.inAnimation = fadeIn
        imageSwitcher!!.outAnimation = fadeOut
        startSlider()

        nav_view.setNavigationItemSelectedListener(this)
        // Change header navigation drawer randomnly
        val navView = findViewById<View>(R.id.nav_view) as NavigationView
        val header = navView.getHeaderView(0)
        val sideNavLayout = header.findViewById<View>(R.id.sideNavLayout) as LinearLayout
        sideNavLayout.setBackgroundResource(HeaderPictures.getBackground())
        makeTextViewsCustomized (header)

    }




    fun makeTextViewsCustomized (header: View){
        val nicknameTextView = header.findViewById<View>(R.id.nickname) as TextView
        val emailTextView = header.findViewById<View>(R.id.email) as TextView
        val settings = PreferenceManager.getDefaultSharedPreferences(this)
        val nameUser = settings.getString("name","")
        val emailUser = settings.getString("email","")

        nicknameTextView.text = "Hola, $nameUser"
        emailTextView.text = "$emailUser"
    }





    private fun displayList() {
        /*
        val version = ArrayList<Version>()
        version.addAll(Version.getList())
        seriesWatchingView.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        seriesWatchingView.adapter = Adapter(version)
        filmsWatchingView.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        filmsWatchingView.adapter = Adapter(version)
        */
    }


    private fun displayListLatestSeries(list : List<Series>) {


        seriesView.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        seriesView.adapter = Adapter(list,"SERIES",this)

    }
    private fun displayListLatestFilms(list : List<Film>) {

        filmsView.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        filmsView.adapter = Adapter(list,"FILMS",this)
    }

    private fun displayListFilmsNoFinished(list : List<Userfilmnofinish>) {

        filmsWatchingView.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        filmsWatchingView.adapter = Adapter(list,"FILMSNOFINISH",this)
    }

    private fun displayListSeriesNoFinished(list : List<Userepisodenofinish>) {

        seriesWatchingView.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        seriesWatchingView.adapter = Adapter(list,"EPISODENOFINISH",this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.index, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var activitySelected : Boolean = false
        var intent : Intent = Intent()

        when (item.itemId) {
            R.id.nav_index -> {

            }
            R.id.nav_search -> {
                intent = Intent(this, SearchActivity::class.java)
                activitySelected = true

            }
            R.id.nav_profile -> {
                intent = Intent(this, ProfileActivity::class.java)
                activitySelected = true
            }
            R.id.nav_logout -> {
                val settings = PreferenceManager.getDefaultSharedPreferences(this)
                val editor = settings.edit()
                editor.clear().commit()
                intent = Intent(this, LoginActivity::class.java)
                activitySelected = true

            }
        }

        if (activitySelected){
            startActivity(intent)
        }

        drawer_layout_index.closeDrawer(GravityCompat.START)
        return true
    }

    var doubleBackToExitPressedOnce = false

    override fun onBackPressed() {
        val extras = intent.extras
        if(extras != null){
            if (doubleBackToExitPressedOnce) {
                finishAffinity()
            }else{
                this.doubleBackToExitPressedOnce = true
                Toast.makeText(this, R.string.back_toast, Toast.LENGTH_SHORT).show()

                Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
            }
        }else{
            super.onBackPressed()
        }

    }

    fun loadData() {
        if (Utility.isOnline(this)) {
            SeriesFacade.getLatestSeries({ list: List<Series> ->
                listSeries = list
                displayListLatestSeries(listSeries!!)
            })
            FilmFacade.getLatestFilms({ listFilm: List<Film> ->
                listFilms = listFilm
                displayListLatestFilms(listFilm)
            })

            UserfilmnofinishFacade.getFilmsNoFinished(this){
                listFilmsNotFinished = it
                displayListFilmsNoFinished(it)
            }

            UserepisodefinishFacade.getEpisodeNoFinished(this){
                listSeriesNotFinished = it
                displayListSeriesNoFinished(it)

            }

        } else {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onResume() {
        loadData()
        super.onResume()
    }

    class ListenerRecyclerView(val pos : Adapter.ViewHolder, val recyclerObject : List<Any>, val context: Context, val type:String) : OnClickListener{

        override fun onClick(v: View?) {
            if(type == "SERIES"){
                recyclerObject as List<Series>
                val  intent = Intent(context,   SerieDetailActivity::class.java)
                intent.putExtra("serie_id", recyclerObject.get(pos.adapterPosition).idSeries)
                Log.d("PULSO", pos.adapterPosition.toString())
                context.startActivity(intent)
            }else if(type == "FILMS"){
                recyclerObject as List<Film>
                Log.d("Pulso Peli", pos.toString())
                val  intent = Intent(context, FilmDetailActivity::class.java)
                Log.d("PULSO", pos.adapterPosition.toString())
                intent.putExtra("film_id",recyclerObject.get(pos.adapterPosition).idFilm)
                context.startActivity(intent)
            }else if(type == "FILMSNOFINISH"){
                recyclerObject as List<Userfilmnofinish>
                val  intent = Intent(context, FilmDetailActivity::class.java)
                intent.putExtra("film_id",recyclerObject.get(pos.adapterPosition).film.idFilm)
                context.startActivity(intent)
            }else{
                recyclerObject as List<Userepisodenofinish>
                val  intent = Intent(context, SerieDetailActivity::class.java)
                intent.putExtra("serie_id",recyclerObject.get(pos.adapterPosition).episode.episodePK.idSeries)
                intent.putExtra("INDEX","INDEX");
                context.startActivity(intent)
            }
        }
    }
    private fun startSlider() {
        timer = Timer()
        timer!!.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    imageSwitcher!!.setImageResource(galeria[posicion])
                    posicion++
                    if (posicion == galeria.size)
                        posicion = 0
                }
            }
        }, 0, DURACION.toLong())
    }

    companion object {
        private val DURACION = 9000
    }
}
