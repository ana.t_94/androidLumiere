package com.inftel.lumiereandroid.View

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import com.inftel.lumiereandroid.Model.entity.Users
import com.inftel.lumiereandroid.R
import android.content.Intent
import android.view.WindowManager
import android.widget.Toast
import com.inftel.lumiereandroid.Presenter.presenter.facade.UserFacade


class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

    fun doRegister(view: View) {
        var user_email = findViewById<View>(R.id.user_email) as EditText
        var password = findViewById<View>(R.id.register_password) as EditText
        var name = findViewById<View>(R.id.register_name) as EditText
        var surname = findViewById<View>(R.id.register_surname) as EditText
        var password2 = findViewById<View>(R.id.register_confirm) as EditText

        if (name.text.toString().trim().equals("") || user_email.text.toString().trim().equals("") || password.text.toString().trim().equals("")) {

            Toast.makeText(this, "All  fields are required", Toast.LENGTH_SHORT).show()
            clear()
        } else {

            if (password.text.toString() == password2.text.toString()) {
                var users = Users(null, name.text.toString(), password.text.toString(), "", surname.text.toString(), user_email.text.toString())

                UserFacade.register(users) {
                    if (it.idUser != null) {
                        Toast.makeText(this, "Register complete successfully", Toast.LENGTH_SHORT).show()
                        goLogin()
                    } else {
                        Toast.makeText(this, "Register failed", Toast.LENGTH_SHORT).show()
                        clear()
                    }
                }
            } else {
                Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show()
                clear()
            }

        }

    }

    fun goLogin(){
        val  intent = Intent(this, LoginActivity::class.java)
        intent.putExtra("PREVIOUS", R.string.title_activity_login)
        startActivity(intent)
    }

    fun cancel(view: View){
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }


    fun clear(){
        var user_email = findViewById<View>(R.id.user_email) as EditText
        var password = findViewById<View>(R.id.register_password) as EditText
        var name = findViewById<View>(R.id.register_name) as EditText
        var surname = findViewById<View>(R.id.register_surname) as EditText
        var password2 = findViewById<View>(R.id.register_confirm) as EditText


        password.setText("")
        user_email.setText("")
        name.setText("")
        surname.setText("")
        password2.setText("")
    }





}
