package com.inftel.lumiereandroid.View

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.*
import com.bumptech.glide.Glide
import com.inftel.lumiereandroid.Presenter.presenter.facade.FilmFacade
import com.inftel.lumiereandroid.R
import com.inftel.lumiereandroid.Utils.HeaderPictures
import kotlinx.android.synthetic.main.activity_film_detail.*
import kotlinx.android.synthetic.main.app_bar_film_detail.*
import kotlinx.android.synthetic.main.app_bar_film_detail.*
import java.math.BigDecimal

//import kotlinx.android.synthetic.main.app_bar_index.*

class FilmDetailActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var film_id : BigDecimal? = null
    var yt_link :String? = null
    var title :String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_film_detail)
        setSupportActionBar(toolbar)
        val extras = intent.extras
        film_id = extras.get("film_id") as BigDecimal

        fabFilm.setOnClickListener { view ->
            /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()*/
           onBackPressed()
        }

        toolbar.setBackgroundColor(Color.parseColor("#3b4451"))
        window.statusBarColor = Color.parseColor("#3b4451")

        setSupportActionBar(toolbar)



        // Change header navigation drawer randomnly
        val navView = findViewById<View>(R.id.nav_view) as NavigationView
        val header = navView.getHeaderView(0)
        val sideNavLayout = header.findViewById<View>(R.id.sideNavLayout) as LinearLayout
        sideNavLayout.setBackgroundResource(HeaderPictures.getBackground())
        makeTextViewsCustomized (header)

        FilmFacade.getFilmById(film_id.toString()) {
            toolbar.setTitle(it.title)
            val sin =  findViewById <View>(R.id.sinopsisTextView) as TextView
            sin.text  = it.sinopsis

            val quoteAuthor = findViewById <View>(R.id.quoteAuthorTextView) as TextView
            quoteAuthor.text = "\"" + it.quote + "\" — " + it.quoteAuthor
            quoteAuthor.setTypeface(quoteAuthor.typeface, Typeface.BOLD_ITALIC)

            val yearDuration =  findViewById <View>(R.id.yearDurationTextView) as TextView
            val minutes :Int = it.duration.toInt()
            yearDuration.text = it.year.toString() + " — " + minutes.toString()  + " min"

            val image =  findViewById <View>(R.id.filmDetailImage)  as ImageView

            Glide.with(this).load(it.imageNetflix).into(image)

            yt_link = it.ytLink
            title = it.title
        }



        val toggle = ActionBarDrawerToggle(
                this, drawer_layout_film_detail, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout_film_detail.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout_film_detail.isDrawerOpen(GravityCompat.START)) {
            drawer_layout_film_detail.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.film_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }
    fun makeTextViewsCustomized (header: View){
        val nicknameTextView = header.findViewById<View>(R.id.nickname) as TextView
        val emailTextView = header.findViewById<View>(R.id.email) as TextView

        val settings = PreferenceManager.getDefaultSharedPreferences(this)
        val nameUser = settings.getString("name","")
        val emailUser = settings.getString("email","")

        nicknameTextView.text = "Hola, $nameUser"
        emailTextView.text = "$emailUser"
    }

    fun startPlayer(view: View){
        var intent = Intent(this, PlayerActivity::class.java)
        intent.putExtra("title",title)
        intent.putExtra("previous","film")
        intent.putExtra("id_film",film_id.toString())

        startActivity(intent)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var activitySelected : Boolean = false
        var intent : Intent = Intent()

        when (item.itemId) {
            R.id.nav_index -> {
                intent = Intent(this, IndexActivity::class.java)
                activitySelected = true
            }
            R.id.nav_search -> {
                intent = Intent(this, SearchActivity::class.java)
                activitySelected = true

            }
            R.id.nav_profile -> {
                intent = Intent(this, ProfileActivity::class.java)
                activitySelected = true

            }
            R.id.nav_logout -> {
                val settings = PreferenceManager.getDefaultSharedPreferences(this)
                val editor = settings.edit()
                editor.clear().commit()
                intent = Intent(this, LoginActivity::class.java)
                activitySelected = true
            }
        }

        if (activitySelected){
            startActivity(intent)
        }

        drawer_layout_film_detail.closeDrawer(GravityCompat.START)
        return true
    }

    fun onClickTrailer(view:View){

        val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$yt_link"))
        val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=$yt_link"))
        try {
            startActivity(appIntent)
        } catch (e: ActivityNotFoundException) {
            startActivity(webIntent)
        }
    }
}
