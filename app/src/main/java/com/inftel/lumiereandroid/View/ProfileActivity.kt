package com.inftel.lumiereandroid.View

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.inftel.lumiereandroid.Model.entity.Users
import com.inftel.lumiereandroid.Presenter.presenter.facade.UserFacade
import com.inftel.lumiereandroid.R
import com.inftel.lumiereandroid.Utils.HeaderPictures
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.app_bar_profile.*
import kotlinx.android.synthetic.main.content_profile.*


class ProfileActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setSupportActionBar(toolbar)
        setTags()

        toolbar.setBackgroundColor(Color.parseColor("#3b4451"))
        window.statusBarColor = Color.parseColor("#3b4451")

        setSupportActionBar(toolbar)


        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        // Change header navigation drawer randomnly
        val navView = findViewById<View>(R.id.nav_view) as NavigationView
        val header = navView.getHeaderView(0)
        val sideNavLayout = header.findViewById<View>(R.id.sideNavLayout) as LinearLayout
        sideNavLayout.setBackgroundResource(HeaderPictures.getBackground())
        makeTextViewsCustomized (header)

    }




    fun makeTextViewsCustomized (header: View){
        val nicknameTextView = header.findViewById<View>(R.id.nickname) as TextView
        val emailTextView = header.findViewById<View>(R.id.email) as TextView
        val settings = PreferenceManager.getDefaultSharedPreferences(this)
        val nameUser = settings.getString("name","")
        val emailUser = settings.getString("email","")

        nicknameTextView.text = "Hola, $nameUser"
        emailTextView.text = "$emailUser"
    }


    fun setTags () {
        val settings = PreferenceManager.getDefaultSharedPreferences(this)
        val userName = settings.getString("name", "")
        val userSurname = settings.getString("surname", "")
        val userEmail = settings.getString("email", "")
        val nameProfile = findViewById<View>(R.id.nameProfile) as TextView
        val emailProfile = findViewById<View>(R.id.user_email) as TextView
        nameProfile.text = userName + " " + userSurname
        emailProfile.text = userEmail


    }
    fun editProfile (view: View) {
        val settings= PreferenceManager.getDefaultSharedPreferences(this)
        val editor = settings.edit()
        val idUser = settings.getString("idUser", "").toBigDecimal()
        val userSurname = settings.getString("surname", "")

        var password = findViewById<View>(R.id.profile_password) as EditText
        var passwordConfirm = findViewById<View>(R.id.profile_confirm) as EditText
        if(password.text.toString() == ""){
            Toast.makeText(this, "You must write something!", Toast.LENGTH_SHORT).show()
        }else {
            if (password.text.toString() == passwordConfirm.text.toString()) {
                var userEdit = Users(idUser, this.nameProfile.text.toString(), password.text.toString(), "", userSurname, this.user_email.text.toString())

                UserFacade.updateUser(userEdit) {

                    if (it.idUser != null) {
                        Toast.makeText(this, "Profile edit successfully", Toast.LENGTH_SHORT).show()
                        editor.clear().commit()
                        goLogin()
                    } else {
                        Toast.makeText(this, "Edit profile failed", Toast.LENGTH_SHORT).show()

                    }
                }
            } else {
                Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show()

            }
        }


    }

    fun goLogin(){
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    fun goIndex (view: View) {
        val  intent = Intent(this, IndexActivity::class.java)
        startActivity(intent)
    }




    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.profile, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var activitySelected : Boolean = false
        var intent : Intent = Intent()

        when (item.itemId) {
            R.id.nav_index -> {
                intent = Intent(this, IndexActivity::class.java)
                activitySelected = true
            }
            R.id.nav_search -> {
                intent = Intent(this, SearchActivity::class.java)
                activitySelected = true

            }
            R.id.nav_profile -> {

            }
            R.id.nav_logout -> {
                val settings = PreferenceManager.getDefaultSharedPreferences(this)
                val editor = settings.edit()
                editor.clear().commit()
                intent = Intent(this, LoginActivity::class.java)
                activitySelected = true
            }
        }

        if (activitySelected){
            startActivity(intent)
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
