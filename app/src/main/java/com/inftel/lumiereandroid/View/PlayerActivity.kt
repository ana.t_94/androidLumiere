package com.inftel.lumiereandroid.View

import android.content.pm.ActivityInfo
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.MediaController
import android.widget.VideoView
import com.inftel.lumiereandroid.R
import android.content.Intent
import android.media.MediaPlayer.OnPreparedListener
import android.media.MediaPlayer
import android.media.MediaPlayer.OnSeekCompleteListener
import com.inftel.lumiereandroid.Presenter.presenter.facade.FilmFacade
import com.inftel.lumiereandroid.Presenter.presenter.facade.SeriesFacade
import com.inftel.lumiereandroid.R.raw.video
import kotlinx.android.synthetic.main.activity_player.*


class PlayerActivity : AppCompatActivity() {
    var videoPath = ""
    var cont = 0
    var id_Film : String = "null"
    var id_Serie : String = "null"
    var id_episode :String = "null"
    var id_season :String = "null"
    var TYPE = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        val intent = intent
        var title = intent.getStringExtra("title")
        var previous = intent.getStringExtra("previous")

        title = "Architect of Destruction"
        videoPath = "http://192.168.1.113:8080/filmController/stream/$title"

        if(previous == "film"){
            TYPE = "film"
            Log.d("Hook", "Título película: " + title.toString())
            id_Film = intent.getStringExtra("id_film")
            Log.d("Hook", "ID película: " + id_Film.toString())
             FilmFacade.getTimeFilmNoFinished(this, id_Film){
                 val videoView = findViewById<VideoView>(R.id.movieVideoView)
                 val uri = Uri.parse(videoPath)
                 videoView.setVideoURI(uri)
                 videoView.setOnPreparedListener( { mp ->
                     mp.seekTo(it.toInt())})
                 videoView.start()
                 val mediaController = MediaController(this)
                 videoView.setMediaController(mediaController)
                 mediaController.setAnchorView(videoView)
             }
        }else if(previous == "serie") {
            TYPE = "serie"
            id_Serie = intent.getStringExtra("id_serie")
            id_episode = intent.getStringExtra("id_episode")
            id_season = intent.getStringExtra("id_season")

            SeriesFacade.getTimeEpisode(this, id_Serie,id_episode,id_season) {
                val videoView = findViewById<VideoView>(R.id.movieVideoView)
                val uri = Uri.parse(videoPath)
                videoView.setVideoURI(uri)
                videoView.setOnPreparedListener({ mp ->
                    mp.seekTo(it.toInt())
                })
                videoView.start()
                val mediaController = MediaController(this)
                videoView.setMediaController(mediaController)
                mediaController.setAnchorView(videoView)
            }
        }
    }

    override fun onResume() {
        val videoView = findViewById<VideoView>(R.id.movieVideoView)

        super.onResume()


    }

    override fun onPause() {
        val videoView = findViewById<VideoView>(R.id.movieVideoView)
        Log.d("TIEMPO","MANDO AL SERVIDOR ${videoView.currentPosition.toString()}")
        if(TYPE == "film"){
            FilmFacade.setTimeFilmNoFinished(this,videoView.currentPosition.toString(),id_Film){

            }

        } else {
            SeriesFacade.setTimeEpisodeNoFinished(this,videoView.currentPosition.toString(),id_episode,id_season,id_Serie){

            }
        }
        videoView.pause()
        super.onPause()
    }



}
