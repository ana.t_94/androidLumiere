package com.inftel.lumiereandroid.View

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.inftel.lumiereandroid.R
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.inftel.lumiereandroid.Model.entity.Users
import com.inftel.lumiereandroid.Presenter.presenter.facade.UserFacade
import android.R.id.edit
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Handler
import android.view.WindowManager
import android.widget.VideoView
import com.inftel.lumiereandroid.Presenter.presenter.facade.UserFacade.Companion.doLogin


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        playBackGroundVideo()


        val settings = PreferenceManager.getDefaultSharedPreferences(this)
        val idUser = settings.getString("idUser","")

        if(idUser != ""){
            goIndex()
        }
    }

    fun doClear(view: View){
        clear()
    }

    override fun onResume (){
        super.onResume()
        playBackGroundVideo()
    }

    fun clear(){
        var user_email = findViewById<View>(R.id.user_email) as EditText
        var password = findViewById<View>(R.id.password) as EditText
        user_email.setText("")
        password.setText("")
    }
    fun doLogin(view: View){
        var user_email = findViewById<View>(R.id.user_email) as EditText
        var password = findViewById<View>(R.id.password) as EditText

        if(user_email.text.trim().equals("") || password.text.trim().equals("")){
            Toast.makeText(this@LoginActivity, "Login and password cannot be empty", Toast.LENGTH_LONG).show()
            clear()
        } else {
            Log.d("EMAIL" ,user_email.text.toString())

            doLogin(user_email.text.toString(),password.text.toString()){
                if(it.idUser == null){
                    Toast.makeText(this@LoginActivity, "Email or password not valid", Toast.LENGTH_LONG).show()

                } else {
                    //save user
                    val settings = PreferenceManager.getDefaultSharedPreferences(this)
                    val editor = settings.edit()
                    editor.putString("email", it.email)
                    editor.putString("pass", it.pass)
                    editor.putString("idUser", it.idUser.toString())
                    editor.putString("name", it.name)
                    editor.putString("surname", it.surname)
                    editor.commit()
                    goIndex()
                }
            }
        }
    }

    fun doRegister(view: View){
        val  intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    /*private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        //TODO: Replace this with your own logic
        return password.length > 4
    }*/

    fun goIndex(){
        val  intent = Intent(this, IndexActivity::class.java)
        intent.putExtra("PREVIOUS", R.string.title_activity_login)
        startActivity(intent)
    }

    var doubleBackToExitPressedOnce = false

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity()
        }else{
            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, R.string.back_toast, Toast.LENGTH_SHORT).show()

            Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
        }
    }

    fun playBackGroundVideo () {
        val videoView = findViewById<VideoView>(R.id.videoView)
        videoView.setOnPreparedListener { mp -> mp.isLooping = true }
        val videoPath = "android.resource://" + packageName + "/" + R.raw.video
        val uri = Uri.parse(videoPath)
        videoView.setVideoURI(uri)
        videoView.start()
    }
}
