package com.inftel.lumiereandroid.View

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager

import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import com.inftel.lumiereandroid.Adapter.Adapter
import com.inftel.lumiereandroid.Model.entity.Film
import com.inftel.lumiereandroid.Model.entity.Series
import com.inftel.lumiereandroid.Presenter.presenter.facade.FilmFacade
import com.inftel.lumiereandroid.Presenter.presenter.facade.SeriesFacade

import com.inftel.lumiereandroid.R

import com.inftel.lumiereandroid.Utils.Utility
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.app_bar_search.*
import kotlinx.android.synthetic.main.content_search.*
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.inftel.lumiereandroid.Utils.HeaderPictures


class SearchActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var searcherdWord : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setSupportActionBar(toolbar_search)
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout_search, toolbar_search, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout_search.addDrawerListener(toggle)
        toggle.syncState()

        toolbar_search.setBackgroundColor(Color.parseColor("#3b4451"))
        window.statusBarColor = Color.parseColor("#3b4451")
        nav_view.setNavigationItemSelectedListener(this)
        doTagsInvisibles(this)
        // Change background
        val navView = findViewById<View>(R.id.nav_view) as NavigationView
        val header = navView.getHeaderView(0)
        val sideNavLayout = header.findViewById<View>(R.id.sideNavLayout) as LinearLayout
        sideNavLayout.setBackgroundResource(HeaderPictures.getBackground())
        makeTextViewsCustomized (header)

    }




    fun makeTextViewsCustomized (header: View){
        val nicknameTextView = header.findViewById<View>(R.id.nickname) as TextView
        val emailTextView = header.findViewById<View>(R.id.email) as TextView
        val settings = PreferenceManager.getDefaultSharedPreferences(this)
        val nameUser = settings.getString("name","")
        val emailUser = settings.getString("email","")

        nicknameTextView.text = "Hola, $nameUser"
        emailTextView.text = "$emailUser"
    }

    fun startSearching(view: View) {
        doTagsVisibles(this)
        doNoFoundTagsInvisbles(this)
        Toast.makeText(this@SearchActivity, "It's a beautiful day for watching something!", Toast.LENGTH_SHORT).show();
        if (Utility.isOnline(this)) {
            val SearchBarEditText = findViewById<EditText>(R.id.searchBar)
            val searchedTerm = SearchBarEditText.text.toString()
            searcherdWord = searchedTerm
            Toast.makeText(this@SearchActivity, searchedTerm, Toast.LENGTH_SHORT).show();

            SeriesFacade.getSeriesByTitle(searchedTerm, { listSeries: List<Series> ->
                displaySeriesByTitle(listSeries)

            })
            FilmFacade.getFilmsByTitle(searchedTerm, { listFilm: List<Film> ->
                displayFilmsByTitle(listFilm)
            })
            FilmFacade.getFilmsByDirector(searchedTerm, { listFilm: List<Film> ->
                displayFilmsByDirector(listFilm)
            })

            // Check if no view has focus:
            if (view != null) {
                val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }

        } else {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show()
        }

    }


    private fun displaySeriesByTitle(list: List<Series>) {
        if (list.size == 0) {
            seriesNotFoundTextView.visibility = View.VISIBLE
        }
        seriesByTitleView.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        seriesByTitleView.adapter = Adapter(list, "SERIES", this)
        val textView =  findViewById <TextView>(R.id.seriesByTitleTextView) as TextView
        textView.text = "Series by " + searcherdWord
    }

    private fun displayFilmsByTitle(list: List<Film>) {
        if (list.size == 0) {
            filmsNotFoundTextView.visibility = View.VISIBLE
        }
        filmsByTitleView.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        filmsByTitleView.adapter = Adapter(list, "FILMS", this)
        val textView =  findViewById <TextView>(R.id.filmsByTitleTextView) as TextView
        textView.text =  "Series by " + searcherdWord
    }


    private fun displayFilmsByDirector(list: List<Film>) {
        if (list.size == 0) {
            directorsNotFoundTextView.visibility = View.VISIBLE
        }
        filmsByDirectorView.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        filmsByDirectorView.adapter = Adapter(list, "FILMS", this)
        val textView =  findViewById <TextView>(R.id.filmsByDirectorTextView) as TextView
        textView.text =  "Film with director " + searcherdWord
    }

    private fun doTagsInvisibles(view: SearchActivity) {

        seriesByTitleTextView.visibility = View.GONE
        filmsByTitleTextView.visibility = View.GONE
        filmsByDirectorTextView.visibility = View.GONE
        seriesNotFoundTextView.visibility = View.GONE
        filmsNotFoundTextView.visibility = View.GONE
        directorsNotFoundTextView.visibility = View.GONE

    }

    private fun doTagsVisibles(view: SearchActivity) {
        seriesByTitleTextView.visibility = View.VISIBLE
        filmsByTitleTextView.visibility = View.VISIBLE
        filmsByDirectorTextView.visibility = View.VISIBLE
    }

    private fun doNoFoundTagsInvisbles(view: SearchActivity) {
        seriesNotFoundTextView.visibility = View.GONE
        filmsNotFoundTextView.visibility = View.GONE
        directorsNotFoundTextView.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (drawer_layout_search.isDrawerOpen(GravityCompat.START)) {
            drawer_layout_search.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.search, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        var activitySelected: Boolean = false
        var intent: Intent = Intent()
        when (item.itemId) {
            R.id.nav_index -> {
                intent = Intent(this, IndexActivity::class.java)
                activitySelected = true

            }
            R.id.nav_search -> {

            }
            R.id.nav_profile -> {
                intent = Intent(this, ProfileActivity::class.java)
                activitySelected = true


            }
            R.id.nav_logout -> {
                val settings = PreferenceManager.getDefaultSharedPreferences(this)
                val editor = settings.edit()
                editor.clear().commit()
                intent = Intent(this, LoginActivity::class.java)
                activitySelected = true
            }

        }

        if (activitySelected) {
            startActivity(intent)
        }


        drawer_layout_search.closeDrawer(GravityCompat.START)
        return true
    }
}
