package com.inftel.lumiereandroid.View

import android.app.ListActivity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.inftel.lumiereandroid.Adapter.ListAdapter
import com.inftel.lumiereandroid.Model.entity.Episode
import com.inftel.lumiereandroid.Model.entity.Season
import com.inftel.lumiereandroid.Model.entity.Series
import com.inftel.lumiereandroid.Presenter.presenter.facade.SeriesFacade
import com.inftel.lumiereandroid.Presenter.presenter.facade.SeriesFacade.Companion.getEpisodesById
import com.inftel.lumiereandroid.Presenter.presenter.facade.SeriesFacade.Companion.getSerieById
import kotlinx.android.synthetic.main.activity_serie_detail.*
import kotlinx.android.synthetic.main.app_bar_serie_detail.*
import com.inftel.lumiereandroid.R
import com.inftel.lumiereandroid.Utils.HeaderPictures
import java.math.BigDecimal

//import kotlinx.android.synthetic.main.app_bar_film_detail.*

class SerieDetailActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemClickListener {


    var serie_id : BigDecimal? = null
    var yt_link : String? = null
    var title : String? = null
    var listEpisodes : List<Episode>? = null
    private var mAdapter: ListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_serie_detail)
        setSupportActionBar(toolbar)

        fabSerie.setOnClickListener { view ->
            onBackPressed()
        }

        toolbar.setBackgroundColor(Color.parseColor("#3b4451"))
        window.statusBarColor = Color.parseColor("#3b4451")

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        // Change background
        val navView = findViewById<View>(R.id.nav_view) as NavigationView
        val header = navView.getHeaderView(0)
        val sideNavLayout = header.findViewById<View>(R.id.sideNavLayout) as LinearLayout
        sideNavLayout.setBackgroundResource(HeaderPictures.getBackground())
        makeTextViewsCustomized (header)

        val extras = intent.extras
        serie_id = extras.get("serie_id") as BigDecimal

        getSerieById(serie_id.toString()){
            toolbar.setTitle(it.title)
            val sin =  findViewById <View>(R.id.sinopsisTextView) as TextView
            sin.text  = it.sinopsis

            val quoteAuthor = findViewById <View>(R.id.quoteAuthorTextView) as TextView
            quoteAuthor.text = "\"" + it.quote + "\" — " + it.quoteAuthor
            quoteAuthor.setTypeface(quoteAuthor.typeface, Typeface.BOLD_ITALIC)

            val yearDuration =  findViewById <View>(R.id.yearTextView) as TextView
            yearDuration.text = it.year.toString()

            val image =  findViewById <View>(R.id.serieDetailImage)  as ImageView

            Glide.with(this).load(it.imageNetflix).into(image)

            yt_link = it.ytLink
            title = it.title

        }


        getEpisodesById(serie_id.toString()){
            mAdapter = ListAdapter(this)
            for(i in 0..it.lastIndex){
                val header = it[i].season.numberSeason.toString()
                if(i == 0){
                    mAdapter?.addSectionHeaderItem("Season #$header")
                }else if(it[i].season.numberSeason != it[i-1].season.numberSeason){
                    mAdapter?.addSectionHeaderItem("Season #$header")
                }
                mAdapter?.addItem(it[i].title)
            }
            listEpisodes = it
            val chapterList = findViewById<ListView>(R.id.chaptersListView) as ListView
            chapterList.adapter = mAdapter
            chapterList.setOnItemClickListener(this)

        }



        /*//Chapter list
        mAdapter = ListAdapter(this)


        for (i in 1..29) {
            mAdapter?.addItem("Row Item #$i")
            if (i % 4 == 0) {
                mAdapter?.addSectionHeaderItem("Section #$i")
            }
        }

        val chapterList = findViewById<ListView>(R.id.chaptersListView) as ListView
        chapterList.adapter = mAdapter

        //Clickable chapter
        chapterList.setOnItemClickListener(AdapterView.OnItemClickListener { adapterView, view, position, l ->
            if (mAdapter?.getItemViewType(position) !== 1) {
                Log.d("APP", "POSITION: ->" + mAdapter?.getPosition(position))
            }
        })*/


    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.serie_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var activitySelected : Boolean = false
        var intent : Intent = Intent()

        when (item.itemId) {
            R.id.nav_index -> {
                intent = Intent(this, IndexActivity::class.java)
                activitySelected = true
            }
            R.id.nav_search -> {
                intent = Intent(this, SearchActivity::class.java)
                activitySelected = true

            }
            R.id.nav_profile -> {
                intent = Intent(this, ProfileActivity::class.java)
                activitySelected = true
            }
            R.id.nav_logout -> {
                val settings = PreferenceManager.getDefaultSharedPreferences(this)
                val editor = settings.edit()
                editor.clear().commit()
                intent = Intent(this, LoginActivity::class.java)
                activitySelected = true
            }
        }

        if (activitySelected){
            startActivity(intent)
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
    fun makeTextViewsCustomized (header: View){
        val nicknameTextView = header.findViewById<View>(R.id.nickname) as TextView
        val emailTextView = header.findViewById<View>(R.id.email) as TextView
        val settings = PreferenceManager.getDefaultSharedPreferences(this)
        val nameUser = settings.getString("name","")
        val emailUser = settings.getString("email","")

        nicknameTextView.text = "Hola, $nameUser"
        emailTextView.text = "$emailUser"
    }
    fun onClickTrailer(view:View){

        val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$yt_link"))
        val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=$yt_link"))
        try {
            startActivity(appIntent)
        } catch (e: ActivityNotFoundException) {
            startActivity(webIntent)
        }
    }


    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (mAdapter?.getItemViewType(position) !== 1) {
            Log.d("Hook", "POSITION: ->" + mAdapter?.getPosition(position))
            var intent = Intent(this, PlayerActivity::class.java)

            var epi = listEpisodes?.get(position)
            intent.putExtra("title", title)
            intent.putExtra("previous","serie")
            intent.putExtra("id_season",epi?.episodePK?.idSeason.toString())
            intent.putExtra("id_episode",epi?.episodePK?.idEpisode.toString())
            intent.putExtra("id_serie", epi?.episodePK?.idSeries.toString())

            startActivity(intent)
        }
    }


}


