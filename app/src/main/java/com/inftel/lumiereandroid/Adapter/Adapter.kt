package com.inftel.lumiereandroid.Adapter

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.inftel.lumiereandroid.Model.entity.Film
import com.inftel.lumiereandroid.Model.entity.Series
import com.inftel.lumiereandroid.Model.entity.Userepisodenofinish
import com.inftel.lumiereandroid.Model.entity.Userfilmnofinish
import com.inftel.lumiereandroid.Model.view.RecyclerObject
import com.inftel.lumiereandroid.R
import com.inftel.lumiereandroid.Model.view.Version
import com.inftel.lumiereandroid.View.IndexActivity
import com.inftel.lumiereandroid.View.ProfileActivity

class Adapter( val recyclerObject: List<Any>,val type : String,val context: Context) : RecyclerView.Adapter<Adapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false),
                recyclerObject,type,context)

    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(type == "SERIES"){
            recyclerObject as List<Series>
            holder.bindItems(recyclerObject[position])
        }else if(type == "FILMS"){
            recyclerObject as List<Film>
            holder.bindItems(recyclerObject[position])
        } else if(type == "FILMSNOFINISH"){
            recyclerObject as List<Userfilmnofinish>
            holder.bindItems(recyclerObject[position])
        } else {
            recyclerObject as List<Userepisodenofinish>
            holder.bindItems(recyclerObject[position])
        }
    }

    override fun getItemCount(): Int {
        return recyclerObject.size
    }

    class ViewHolder(itemView: View,recyclerObject: List<Any>,type:String,context: Context) : RecyclerView.ViewHolder(itemView) {
        init {
            val listener  = IndexActivity.ListenerRecyclerView(this, recyclerObject, context, type)
            itemView.setOnClickListener(listener)
        }
        fun bindItems(version: Any) {
            if(version is Series){
                val textView = itemView.findViewById<TextView>(R.id.titlePoster)
                val imageView = itemView.findViewById<ImageView>(R.id.imagePoster)
                textView.text = version.title
                Glide.with(itemView.context).load(version.imagePoster).into(imageView)
            }else if(version is Film){
                val textView = itemView.findViewById<TextView>(R.id.titlePoster)
                val imageView = itemView.findViewById<ImageView>(R.id.imagePoster)
                textView.text = version.title
                Glide.with(itemView.context).load(version.imagePoster).into(imageView)
            }else if(version is Userepisodenofinish){
                val textView = itemView.findViewById<TextView>(R.id.titlePoster)
                val imageView = itemView.findViewById<ImageView>(R.id.imagePoster)
                textView.text = version.episode.title
                Glide.with(itemView.context).load(version.episode.season.series.imagePoster).into(imageView)
            }else if(version is Userfilmnofinish){
                val textView = itemView.findViewById<TextView>(R.id.titlePoster)
                val imageView = itemView.findViewById<ImageView>(R.id.imagePoster)
                textView.text = version.film.title
                Glide.with(itemView.context).load(version.film.imagePoster).into(imageView)
            }else if(version is Userepisodenofinish){
            }
        }
    }

}