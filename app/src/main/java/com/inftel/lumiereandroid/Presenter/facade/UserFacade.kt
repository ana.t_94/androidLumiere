package com.inftel.lumiereandroid.Presenter.presenter.facade

import android.util.Log
import com.inftel.lumiereandroid.Model.entity.Users
import com.inftel.lumiereandroid.Presenter.facade.RetrofitClient
import com.inftel.lumiereandroid.Presenter.presenter.service.UserService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class UserFacade{
    companion object {
        var urlBase = "http://192.168.183.58:8080/userController/"
        fun doLogin(email: String, pass: String,body: (Users) -> Unit) {
            val retrofit = Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()
            val filmsService = retrofit.create(UserService::class.java)
            val callList = filmsService.login(email,pass)
            callList.enqueue(object : Callback<Users> {

                override fun onFailure(call: Call<Users>?, t: Throwable?) {

                }
                override fun onResponse(call: Call<Users>, response: Response<Users>?) {
                    if (response != null && response.isSuccessful) {
                        body(response.body()!!)
                    } else {
                        body(Users(null,null,null,null,null,null))
                    }

                }
            })
        }

        fun register(user:Users,body: (Users) -> Unit){
            val mAPIService = RetrofitClient.getClient(urlBase)!!.create(UserService::class.java)
            mAPIService!!.registrationPost(user).enqueue(object : Callback<Users> {

                override fun onResponse(call: retrofit2.Call<Users>, response: Response<Users>) {
                    Log.i("", "post submitted to API." + response.body()!!)
                    if (response.isSuccessful()) {
                                body(response.body()!!)
                    }
                }

                override fun onFailure(call: retrofit2.Call<Users>, t: Throwable) {

                }

            })

        }
        fun updateUser(user: Users, body: (Users) -> Unit) {
            val mAPIService = RetrofitClient.getClient(urlBase)!!.create(UserService::class.java)
            mAPIService!!.updateUser(user).enqueue(object : Callback<Users> {
                override fun onResponse(call: retrofit2.Call<Users>, response: Response<Users>) {
                    Log.i("", "post submitted to API." + response.body()!!)
                    if (response.isSuccessful()) {
                        body(response.body()!!)
                    }
                }

                override fun onFailure(call: retrofit2.Call<Users>, t: Throwable) {

                }
            })

        }
    }
}
