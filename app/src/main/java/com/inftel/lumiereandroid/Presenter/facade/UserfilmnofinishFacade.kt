package com.inftel.lumiereandroid.Presenter.facade

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.inftel.lumiereandroid.Model.entity.Userfilmnofinish
import com.inftel.lumiereandroid.Model.entity.Users
import com.inftel.lumiereandroid.Presenter.presenter.service.UserService
import com.inftel.lumiereandroid.Presenter.service.UserfilmnofinishService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class UserfilmnofinishFacade{
    companion object {
        var urlBase = "http://192.168.183.58:8080/filmController/"
        fun getFilmsNoFinished(context: Context, body: (List<Userfilmnofinish>) -> Unit) {
            val retrofit = Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()
            val filmsnofinishService = retrofit.create(UserfilmnofinishService::class.java)
            val settings = PreferenceManager.getDefaultSharedPreferences(context)
            val idUser = settings.getString("idUser","")
            val callList = filmsnofinishService.getFilmNotFinished(idUser)
            callList.enqueue(object : Callback<List<Userfilmnofinish>> {

                override fun onFailure(call: Call<List<Userfilmnofinish>>?, t: Throwable?) {

                }

                override fun onResponse(call: Call<List<Userfilmnofinish>>, response: Response<List<Userfilmnofinish>>?) {
                    Log.d("PASO","ONRESPONSE")

                    if (response != null && response.isSuccessful) {
                        body(response.body()!!)
                    }
                }
            })

        }
    }
}