package com.inftel.lumiereandroid.Presenter.service

import com.inftel.lumiereandroid.Model.entity.Userfilmnofinish
import com.inftel.lumiereandroid.Model.entity.Users
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface UserfilmnofinishService {

    @GET("getFilmNotFinished/{userId}")
    fun getFilmNotFinished(@Path("userId") userId:String): Call<List<Userfilmnofinish>>




}