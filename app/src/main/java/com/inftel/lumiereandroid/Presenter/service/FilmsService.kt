package com.inftel.lumiereandroid.Presenter.presenter.service

import com.inftel.lumiereandroid.Model.entity.Film
import com.inftel.lumiereandroid.Model.entity.Userfilmnofinish
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface FilmsService {

    @GET("getLastestFilms")
    fun getLatestFilms(): Call<List<Film>>

    @GET("getFilmById/{filmId}")
    fun getFilmById(@Path("filmId") filmId:String) : Call<Film>


    @GET("getFilmsByTitle/{searchedTerm}")
    fun getFilmsByTitle(@Path("searchedTerm") searchedTerm: String ): Call<List<Film>>


    @GET("getFilmsByDirector/{searchedTerm}")
    fun getFilmsByDirector(@Path("searchedTerm")  searchedTerm: String): Call<List<Film>>

    @GET("getFilmNotFinished/{userId}")
    fun getFilmNotFinished(@Path("userId")  userId: String): Call<List<Userfilmnofinish>>

    @GET("filmViewUpdate/{idFilm}/{idUser}")
    fun getTimeNotFinishedTime(@Path("idFilm")  idFilm: String,@Path("idUser")  idUser: String): Call<String>

    @GET("filmViewNoFinish/{idFilm}/{idUser}/{time}")
    fun setTimeNotFinishedTime(@Path("idFilm")  idFilm: String,@Path("idUser")  idUser: String,@Path("time")  time: String): Call<Unit>
}