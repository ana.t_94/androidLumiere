package com.inftel.lumiereandroid.Presenter.presenter.facade

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.inftel.lumiereandroid.Model.entity.Film
import com.inftel.lumiereandroid.Presenter.presenter.service.FilmsService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FilmFacade{
    companion object {
        var urlBase = "http://192.168.183.58:8080/filmController/"
        fun getLatestFilms(body: (List<Film>) -> Unit) {
            val retrofit = Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()

            val filmsService = retrofit.create(FilmsService::class.java)
            val callList = filmsService.getLatestFilms()

            callList.enqueue(object : Callback<List<Film>> {

                override fun onFailure(call: Call<List<Film>>?, t: Throwable?) {
                    Log.d("ERROR", t?.message)
                }

                override fun onResponse(call: Call<List<Film>>, response: Response<List<Film>>?) {
                    if (response != null && response.isSuccessful) {
                        Log.d("OK", "BIEN")
                        body(response.body()!!)
                    } else {
                        Log.d("ERROR", "REQUEST ERROR")
                    }
                }
            })

        }

        fun getFilmById(id: String,body: (Film) -> Unit){
            val retrofit = Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()

            val filmsService = retrofit.create(FilmsService::class.java)
            val callList = filmsService.getFilmById(id)

            callList.enqueue(object : Callback<Film> {

                override fun onFailure(call: Call<Film>?, t: Throwable?) {
                    Log.d("ERROR", t?.message)
                }

                override fun onResponse(call: Call<Film>, response: Response<Film>?) {
                    Log.d("RESPUESTA", "BIEN")
                    if (response != null && response.isSuccessful) {
                        body(response.body()!!)
                    }else{
                        Log.d("RESPUESTA", "NO BIEN")

                    }
                }
            })
        }
        fun getFilmsByTitle(searchedTerm: String, body: (List<Film>) -> Unit) {
            val retrofit = Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()
            val filmsService = retrofit.create(FilmsService::class.java)
            val callList = filmsService.getFilmsByTitle(searchedTerm)

            callList.enqueue(object : Callback<List<Film>> {

                override fun onFailure(call: Call<List<Film>>?, t: Throwable?) {
                    Log.d("ERROR", t?.message)
                }

                override fun onResponse(call: Call<List<Film>>, response: Response<List<Film>>?) {
                    if (response != null && response.isSuccessful) {
                        Log.d("OK", "BIEN")
                        body(response.body()!!)
                    } else {
                        Log.d("ERROR", "REQUEST ERROR")
                    }
                }
            })

        }

        fun getFilmsByDirector(searchedTerm: String, body: (List<Film>) -> Unit) {
            val retrofit = Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()

            val filmsService = retrofit.create(FilmsService::class.java)
            val callList = filmsService.getFilmsByDirector(searchedTerm)

            callList.enqueue(object : Callback<List<Film>> {

                override fun onFailure(call: Call<List<Film>>?, t: Throwable?) {
                    Log.d("ERROR", t?.message)
                }

                override fun onResponse(call: Call<List<Film>>, response: Response<List<Film>>?) {
                    if (response != null && response.isSuccessful) {
                        Log.d("OK", "BIEN")
                        body(response.body()!!)
                    } else {
                        Log.d("ERROR", "REQUEST ERROR")
                    }
                }
            })

        }

        fun getTimeFilmNoFinished(context: Context, filmId: String, body: (String) -> Unit) {
            val retrofit = Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()
            val settings = PreferenceManager.getDefaultSharedPreferences(context)
            val idUser = settings.getString("idUser","")

            val filmsService = retrofit.create(FilmsService::class.java)

            val callList = filmsService.getTimeNotFinishedTime(filmId,idUser)

            callList.enqueue(object : Callback<String> {

                override fun onFailure(call: Call<String>?, t: Throwable?) {
                    Log.d("TIME","FALLO1")

                    body("0")
                }

                override fun onResponse(call: Call<String>, response: Response<String>?) {
                    if (response != null && response.isSuccessful) {

                        body(response.body()!!)
                        Log.d("TIME","TIME BIEN")


                        Log.d("TIME",response.body())
                    } else {
                        Log.d("TIME","FALLO2")
                    }
                }
            })

        }

        fun setTimeFilmNoFinished(context: Context,time:String, filmId: String, body: (Unit) -> Unit) {
            val retrofit = Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()
            val settings = PreferenceManager.getDefaultSharedPreferences(context)
            val idUser = settings.getString("idUser","")

            val filmsService = retrofit.create(FilmsService::class.java)

            val callList = filmsService.setTimeNotFinishedTime(filmId,idUser,time)

            callList.enqueue(object : Callback<Unit> {

                override fun onFailure(call: Call<Unit>?, t: Throwable?) {
                    body(Unit)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                    body(Unit)

                }
            })

        }

    }
}
