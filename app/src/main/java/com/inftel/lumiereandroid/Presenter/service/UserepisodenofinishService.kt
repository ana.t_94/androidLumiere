package com.inftel.lumiereandroid.Presenter.service

import com.inftel.lumiereandroid.Model.entity.Userepisodenofinish
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface UserepisodenofinishService {

    @GET("getEpisodesNotFinished/{userId}")
    fun getFilmNotFinished(@Path("userId") userId:String): Call<List<Userepisodenofinish>>




}