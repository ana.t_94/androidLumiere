package com.inftel.lumiereandroid.Presenter.presenter.service

import com.inftel.lumiereandroid.Model.entity.Episode
import com.inftel.lumiereandroid.Model.entity.Series
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface SeriesService {

    @GET("getLastestSeries")
    fun getLatestSeries(): Call<List<Series>>

    @GET("getSeriesById/{serieId}")
    fun getSeriesById(@Path("serieId") serieId:String): Call<Series>

    @GET ("getSeriesByTitle/{searchedTerm}")
    fun getSeriesByTitle(@Path("searchedTerm") searchedTerm: String): Call<List<Series>>

    @GET ("getSeriesByGenre/{searchedTerm}")
    fun getSeriesByGenre(@Path("searchedTerm") searchedTerm: String): Call<List<Series>>

    @GET ("getEpisodesById/{id_serie}")
    fun getEpisodesById(@Path("id_serie") searchedTerm: String): Call<List<Episode>>

    @GET("serieViewUpdate/{idEpisode}/{idSeason}/{idSerie}/{idUser}")
    fun getTimeEpisode(@Path("idEpisode") idEpisode: String,@Path("idSeason") idSeason: String,@Path("idSerie") idSerie: String,@Path("idUser") idUser: String): Call<String>

    @GET("serieViewNoFinish/{idEpisode}/{idSeason}/{idSerie}/{idUser}/{time}")
    fun setTimeNotFinishedEpisode (@Path("idEpisode")  idEpisode: String,@Path("idSeason")  idSeason: String,@Path("idSerie")  idSerie: String,@Path("idUser")  idUser: String,@Path("time")  time: String): Call<Unit>

}