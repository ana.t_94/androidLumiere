package com.inftel.lumiereandroid.Presenter.presenter.facade

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.inftel.lumiereandroid.Model.entity.Episode
import com.inftel.lumiereandroid.Model.entity.Series
import com.inftel.lumiereandroid.Presenter.presenter.service.FilmsService
import com.inftel.lumiereandroid.Presenter.presenter.service.SeriesService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Path

class SeriesFacade{


    companion object {
        var urlBase = "http://192.168.183.58:8080/seriesController/"
        fun getLatestSeries(body: (List<Series>) -> Unit){
            val retrofit  =  Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()

            val seriesService = retrofit.create(SeriesService::class.java)
            val callList = seriesService.getLatestSeries()

            callList.enqueue(object : Callback<List<Series>> {

                override fun onFailure(call: Call<List<Series>>?, t: Throwable?) {
                    Log.d("ERROR",t?.message)
                }
                override fun onResponse(call: Call<List<Series>>, response: Response<List<Series>>?) {
                    if (response != null && response.isSuccessful) {

                        body(response.body()!!)
                    } else {
                    }
                }
            })

        }

        fun getSerieById(serie_id:String,body: (Series) -> Unit){
            val retrofit  =  Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()

            val seriesService = retrofit.create(SeriesService::class.java)
            val callList = seriesService.getSeriesById(serie_id)

            callList.enqueue(object : Callback<Series> {

                override fun onFailure(call: Call<Series>?, t: Throwable?) {
                    Log.d("ERROR",t?.message)
                }

                override fun onResponse(call: Call<Series>, response: Response<Series>?) {
                    if (response != null && response.isSuccessful) {
                        body(response.body()!!)
                    } else {
                    }
                }
            })

        }

        fun getEpisodesById(searchedTerm: String, body: (List<Episode>) -> Unit){
            val retrofit  =  Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()

            val seriesService = retrofit.create(SeriesService::class.java)
            val callList = seriesService.getEpisodesById(searchedTerm)

            callList.enqueue(object : Callback<List<Episode>> {

                override fun onFailure(call: Call<List<Episode>>?, t: Throwable?) {
                    Log.d("ERROR",t?.message)
                }

                override fun onResponse(call: Call<List<Episode>>, response: Response<List<Episode>>?) {
                    if (response != null && response.isSuccessful) {
                        Log.d("OK", "BIEN")
                        body(response.body()!!)
                    } else {
                        Log.d("ERROR", "REQUEST ERROR")
                    }
                }
            })

        }

        fun getSeriesByTitle(searchedTerm: String, body: (List<Series>) -> Unit){

            val retrofit  =  Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()
            val seriesService = retrofit.create(SeriesService::class.java)
            val callList = seriesService.getSeriesByTitle(searchedTerm)

            callList.enqueue(object : Callback<List<Series>> {

                override fun onFailure(call: Call<List<Series>>?, t: Throwable?) {
                    Log.d("ERROR",t?.message)
                }

                override fun onResponse(call: Call<List<Series>>, response: Response<List<Series>>?) {
                    if (response != null && response.isSuccessful) {
                        Log.d("OK", "BIEN")
                        body(response.body()!!)
                    } else {
                        Log.d("ERROR", "REQUEST ERROR")
                    }
                }
            })

        }

        fun getSeriesByGenre(genre: String, body: (List<Series>) -> Unit){

            val retrofit  =  Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()
            val seriesService = retrofit.create(SeriesService::class.java)
            val callList = seriesService.getSeriesByGenre(genre)

            callList.enqueue(object : Callback<List<Series>> {

                override fun onFailure(call: Call<List<Series>>?, t: Throwable?) {
                    Log.d("ERROR",t?.message)
                }

                override fun onResponse(call: Call<List<Series>>, response: Response<List<Series>>?) {
                    if (response != null && response.isSuccessful) {
                        Log.d("OK", "BIEN")
                        body(response.body()!!)
                    } else {
                        Log.d("ERROR", "REQUEST ERROR")
                    }
                }
            })

        }

        fun getTimeEpisode(context: Context, idSerie:String, idEpisode: String, idSeason :String, body: (String) -> Unit){

            val retrofit  =  Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()
            val seriesService = retrofit.create(SeriesService::class.java)
            val settings = PreferenceManager.getDefaultSharedPreferences(context)
            val idUser = settings.getString("idUser","")
            val callList = seriesService.getTimeEpisode(idEpisode,idSeason,idSerie,idUser)

            callList.enqueue(object : Callback<String> {

                override fun onFailure(call: Call<String>?, t: Throwable?) {
                    Log.d("ERROR",t?.message)
                    body("0")
                }

                override fun onResponse(call: Call<String>, response: Response<String>?) {
                    if (response != null && response.isSuccessful) {
                        Log.d("RESPONSE","TIME")

                        body(response.body()!!)
                    } else {
                        body("0")
                    }
                }
            })

        }


        fun setTimeEpisodeNoFinished(context: Context,time:String, idEpisode: String,idSeason: String,idSerie: String, body: (Unit) -> Unit) {
            val retrofit = Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()
            val settings = PreferenceManager.getDefaultSharedPreferences(context)
            val idUser = settings.getString("idUser","")

            val episodeService = retrofit.create(SeriesService::class.java)

            val callList = episodeService.setTimeNotFinishedEpisode(idEpisode,idSeason,idSerie,idUser,time)

            callList.enqueue(object : Callback<Unit> {

                override fun onFailure(call: Call<Unit>?, t: Throwable?) {
                    body(Unit)
                }

                override fun onResponse(call: Call<Unit>, response: Response<Unit>?) {
                    body(Unit)

                }
            })

        }



    }
}

