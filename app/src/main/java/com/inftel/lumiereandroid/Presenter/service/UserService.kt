package com.inftel.lumiereandroid.Presenter.presenter.service

import com.inftel.lumiereandroid.Model.entity.Series
import com.inftel.lumiereandroid.Model.entity.Users
import retrofit2.Call
import retrofit2.http.*

interface UserService {

    @GET("login/{email}/{pass}")
    fun login(@Path("email") email:String,@Path("pass") pass:String): Call<Users>

    @POST("register")
    fun registrationPost(@Body users: Users): Call<Users>

    @PATCH("users/{userId}")
    fun updateUser (@Body users: Users): Call<Users>

}