package com.inftel.lumiereandroid.Presenter.facade

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.inftel.lumiereandroid.Model.entity.Userepisodenofinish
import com.inftel.lumiereandroid.Model.entity.Userfilmnofinish
import com.inftel.lumiereandroid.Presenter.service.UserepisodenofinishService
import com.inftel.lumiereandroid.Presenter.service.UserfilmnofinishService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class UserepisodefinishFacade{
    companion object {
        var urlBase = "http://192.168.183.58:8080/seriesController/"
        fun getEpisodeNoFinished(context: Context, body: (List<Userepisodenofinish>) -> Unit) {
            val retrofit = Retrofit.Builder().baseUrl(urlBase).addConverterFactory(GsonConverterFactory.create()).build()
            val episdenofinishService = retrofit.create(UserepisodenofinishService::class.java)
            val settings = PreferenceManager.getDefaultSharedPreferences(context)
            val idUser = settings.getString("idUser","")
            val callList = episdenofinishService.getFilmNotFinished(idUser)
            callList.enqueue(object : Callback<List<Userepisodenofinish>> {

                override fun onFailure(call: Call<List<Userepisodenofinish>>?, t: Throwable?) {

                }

                override fun onResponse(call: Call<List<Userepisodenofinish>>, response: Response<List<Userepisodenofinish>>?) {
                    Log.d("PASO","ONRESPONSE")
                    if (response != null && response.isSuccessful) {
                        body(response.body()!!)
                    }
                }
            })

        }
    }
}