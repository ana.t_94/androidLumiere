

package com.inftel.lumiereandroid.Utils
import com.inftel.lumiereandroid.R
import java.util.Random

import java.util.*

class HeaderPictures {
    companion object HeaderPictures {

        val backgroundImages = arrayOf(R.mipmap.ic_header_layer_1, R.mipmap.ic_header_layer_2,
                R.mipmap.ic_header_layer_4, R.mipmap.ic_header_layer_5,R.mipmap.ic_header_layer_6,
                R.mipmap.ic_header_layer_7,R.mipmap.ic_header_layer_8,R.mipmap.ic_header_layer_9,
                R.mipmap.ic_header_layer_10,R.mipmap.ic_header_layer_11, R.mipmap.ic_header_layer_12)
        //val backgroundImages = arrayOf(R.mipmap.ic_header_layer_9)

        fun getBackground() : Int {
            return this.backgroundImages[this.rand(0, this.backgroundImages.size)]
        }


        fun rand(from: Int, to: Int) : Int {
            val random : Random = Random()
            return random.nextInt(to - from) + from
        }
    }

}